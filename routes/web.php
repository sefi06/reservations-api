<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('/register','UserController@register');
    $router->post('/login','UserController@login');
});

$router->group([
    'prefix' => 'reservation', 
    'middleware' => 'auth',
], function () use ($router) {
    $router->post('/','ReservationController@create');
    $router->get('/{id}','ReservationController@read');
    $router->put('/{id}','ReservationController@update');
    $router->delete('/{id}','ReservationController@delete');
});