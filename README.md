
# RESTFUL API Reservations

project ini dibangun menggunakan Lumen Microframework by Laravel.

Di dalamnya terdapat beberapa endpoint auth seperti:
- Register
- Login

Dan juga simple CRUD untuk melakukan resrvasi yang sudah divalidasi:
- Create
- Read
- Update
- Delete
## Design Pattern

Proyek ini mengikuti beberapa prinsip dan pola desain umum dalam pengembangan API RESTful, termasuk:

- MVC (Model-View-Controller): Meskipun dalam konteks API "View" kurang relevan, pola MVC tetap diterapkan dimana Model mewakili struktur data, Controller menangani logika aplikasi, dan tampilan digantikan oleh respons JSON.
- Middleware untuk autentikasi JWT: Memastikan bahwa hanya pengguna terautentikasi yang dapat mengakses endpoint tertentu.
- RESTful principles: Menggunakan metode HTTP (GET, POST, PUT, DELETE) untuk operasi CRUD, serta struktur URI yang jelas dan respons status kode yang sesuai.
## Installation

Setelah melakukan clone masuk ke project directory dan buat file .env di root directory, untuk isinya bisa mengambil dari file .env.example lalu sesuaikan konfigurasi databasenya:

```
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=6033
DB_DATABASE=app_db
DB_USERNAME=root
DB_PASSWORD=
```

Lalu jalankan perintah:

```
docker-compose build
docker-compose up -d
```

setelah itu jalankan migrasi database & Generate JWT Secret dengan perintah:

```
docker exec -it reservations_lumen_1 php artisan migrate
docker exec -it reservations_lumen_1 php artisan jwt:secret
```

yang mana reservations_lumen_1 adalah nama dari container aplikasi. Setelah semua proses instalasi dijalankan aplikasi siap digunakan
```
http://localhost:2001
```
## API Documentation

#### Auth

```
{POST} /auth/register

params:
- name
- email
- password


{POST} /auth/login

params:
- email
- password

contoh response:

{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwMDAvYXV0aC9sb2dpbiIsImlhdCI6MTcwOTM2NDYzMywiZXhwIjoxNzA5MzY4MjMzLCJuYmYiOjE3MDkzNjQ2MzMsImp0aSI6ImdndzlWV09ZSjg4RDdrTlkiLCJzdWIiOiIxIiwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.-xI2vWpC0WjDhN3B86NILauSgjAolRxh69U_UwRv8-w",
  "token_type": "bearer",
  "user": {
    "id": 1,
    "name": "sefi",
    "email": "sefi@gmail.com",
    "created_at": "2024-03-02T07:30:27.000000Z",
    "updated_at": "2024-03-02T07:30:27.000000Z"
  },
  "expires_in": 86400
}
```

yang mana di endpoint login ini nanti akan mendapatkan response JWT yang digunakan untuk melakukan authentikasi agar bisa melakukan rservasi 


#### Rsesrvations

```
Header:
- Accept: application/json
- Auth: Bearer token

{POST} /Rsesrvation

Digunakan untuk membuat reservasi yang mana nanti akan mendapatkan nomor antrian berdasarkan tanggal reservasi.

params:
- date (Y-m-d)

=======================================

{PUT} /Rsesrvation/{id}
atau
{POST} /Rsesrvation/{id}

Digunakan untuk mengupdate reservasi berdasarkan ID reservasi yang mana nanti akan mendapatkan nomor antrian baru berdasarkan tanggal reservasi yang diupdate.

params:
- date (Y-m-d)
 
 jika menggunakan method POST maka perlu menambahkan param:

- _method: PUT

=======================================

{DELETE} /Rsesrvation/{id}

Digunakan untuk menghapus reservasi berdasarkan ID reservasi.

=======================================

{GET} /Rsesrvation/{id}

Digunakan untuk melihat reservasi berdasarkan ID reservasi
```