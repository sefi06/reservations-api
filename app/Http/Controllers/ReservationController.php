<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ReservationController extends BaseController
{
    public function create(Request $request)
    {
        $this->validate($request, [
            'date' => 'required|date|after:yesterday|unique:reservations,date,NULL,id,iduser,'.auth()->id(),
        ], [
            'date.after'    => 'The date must be today or later.',
            'date.unique'   => 'You have already created a reservation for this date.',
        ]);

        $data   = [
            'ticket_number' => $this->getTicketNumber($request->input('date')),
            'iduser'        => auth()->id()
        ];

        $request->merge($data);

        $data = Reservation::create($request->all());

        return response()->json([
            'message'   => 'Data added successfully',
            'data'      => $data->fresh() 
        ], 201);
    }

    private function getTicketNumber($date) {
        $maxTicketNumber = Reservation::whereDate('date', $date)->max('ticket_number');

        if(!$maxTicketNumber) {
            $maxTicketNumber    = 0;
        }

        $newTicketNumber    = $maxTicketNumber + 1;

        return $newTicketNumber;
    }

    public function read($id)
    {
        $data = Reservation::where('id', $id)->where('iduser', auth()->id())->first();

        return response()->json([
            'data'      => $data 
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date' => 'required|date|after:yesterday|unique:reservations,date,'.$id.',id,iduser,'.auth()->id(),
        ], [
            'date.after'    => 'The date must be today or later.',
            'date.unique'   => 'You have already created a reservation for this date.',
        ]);

        $data   = Reservation::where('id', $id)->where('iduser', auth()->id())->first();

        $datareq= [
            'ticket_number' => $this->getTicketNumber($request->input('date')),
        ];

        $request->merge($datareq);

        if($data) {
            $data->update($request->all());
        }

        return response()->json([
            'message'   => 'Data updated successfully',
            'data'      => $data 
        ], 201);
    }

    public function delete($id)
    {
        $data = Reservation::where('id', $id)->where('iduser', auth()->id())->first();
        
        $data->delete();

        return response()->json([
            'message' => 'Data deleted successfully'
        ], 201);
    }
}
